from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm

# Create your views here.


@login_required
def receipt_list(request):
    lists = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": lists,
    }
    return render(request, "pages/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "pages/create.html", context)


@login_required
def category_list(request):
    receipts = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "pages/categories.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account,
    }
    return render(request, "pages/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {
        "form": form,
    }

    return render(request, "pages/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "pages/createaccount.html", context)
